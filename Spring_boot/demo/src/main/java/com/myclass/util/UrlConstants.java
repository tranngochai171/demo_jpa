package com.myclass.util;

public class UrlConstants {
	// COMMON URL
	public static final String URL_API = "/api";
	public static final String URL_SEARCH = "/search";
	// HOME URL
	public static final String URL_HOME = "/home";
	// ROLE URL
	public static final String URL_ROLE = "/role";
}
